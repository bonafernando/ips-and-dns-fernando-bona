require 'test_helper'

class DnsControllerTest < ActionDispatch::IntegrationTest

  test "should create dns" do
    assert_difference ->{ Dns.count } => 1, ->{ Hostname.count } => 4  do
      post dns_url, params: { ip: '1.1.1.1', hostnames: ['lorem.com', 'ipsum.com', 'dolor.com', 'amet.com'] }
    end

    assert_response :success
  end

  test "should return error if query without search" do
    get dns_url
    assert_response 400
  end

  test "should get the dns when providing params" do
    get dns_url, params: { include: 'ipsum.com,dolor.com', excluded: 'sit.com' }
    assert_response :success
  end

end
