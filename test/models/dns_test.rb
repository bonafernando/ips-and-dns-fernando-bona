require 'test_helper'

class DnsTest < ActiveSupport::TestCase
  test "should not save Dns without ip" do
    dns = Dns.new
    assert_not dns.save
  end
end
