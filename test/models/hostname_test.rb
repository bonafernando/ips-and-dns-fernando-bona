require 'test_helper'

class HostnameTest < ActiveSupport::TestCase
  test "should not save hostname without name" do
    hostname = Hostname.new
    assert_not hostname.save
  end

  test "should not save hostname without dns" do
    hostname = Hostname.new(name: 'test1')
    assert_not hostname.save
  end

  test "should allow association with dns" do
    local_dns = Dns.create(ip: "dns")
    hostname = Hostname.new(name: 'test2', dns: local_dns)
    assert hostname.save
  end
end
