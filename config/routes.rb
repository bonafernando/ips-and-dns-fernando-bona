Rails.application.routes.draw do
  resources :dns, only: [:create, :index]
end
