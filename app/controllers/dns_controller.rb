class DnsController < ApplicationController

  def create
    @dns = Dns.new(dns_params)
    @dns.build_hostnames params[:hostnames]
    if @dns.save
      render json: { id: @dns.id }
    else
      render json: { error: @dns.errors }, status: 400
    end
  end

  def index
    if !index_params[:include] && !index_params[:exclude]
      render json: { error: 'You must filter your search'}, status: 400
    else
      @dnses = Dns.with_included_hostnames(index_params[:include].split(',')) unless index_params[:include].to_s.empty?
      render json: { total_dns: @dnses.size, dns_records: @dnses }
    end
  end

  private

  def dns_params
    params.permit(:ip)
  end

  def index_params
    params.permit(:include, :exclude)
  end
end
