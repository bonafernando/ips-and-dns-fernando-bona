class Hostname < ApplicationRecord
  belongs_to :dns

  validates_presence_of :name, :dns
end
