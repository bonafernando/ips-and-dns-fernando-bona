class Dns < ApplicationRecord
  has_many :hostnames

  validates_presence_of :ip

  def build_hostnames(names)
    names.each do |name|
      hostnames << Hostname.new(name: name)
    end
  end

  def self.with_included_hostnames(names)
    raw_sql = "
      SELECT DISTINCT dns.id, dns.ip
        FROM dns, hostnames
       WHERE hostnames.dns_id = dns.id
         AND hostnames.name IN (?)
       GROUP BY dns.id
      HAVING count(hostnames.id) = ?
    "
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [raw_sql, names, names.size])
    find_by_sql sql
  end

  def self.with_excluded_hostnames(names)
    raw_sql = "
      SELECT DISTINCT dns.id, dns.ip
        FROM dns, hostnames
       WHERE hostnames.dns_id = dns.id
         AND hostnames.name NOT IN (?)
       GROUP BY dns.id
      HAVING count(hostnames.id) = ?
    "
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [raw_sql, names, names.size])
    find_by_sql sql
  end
end
