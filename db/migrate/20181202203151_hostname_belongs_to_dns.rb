class HostnameBelongsToDns < ActiveRecord::Migration[5.2]
  def change
    change_table :hostnames do |t|
      t.belongs_to :dns, foreign_key: true
    end
  end
end
